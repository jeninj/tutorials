package tinyurl.url;

import org.junit.Assert;
import org.junit.Test;

public class TinyURLArithmeticTest {

    String longURL;
    String shortURL;
    int bias; int length = 7;

    @Test
    public void testgetShortURL() {
        longURL = "https://www.testurl123.com";
        shortURL = "https://miniurl.com/psgptsr";
        bias = 0;
        Assert.assertEquals(shortURL, TinyURLArithmetic.getShortUrl(longURL,bias,length));

        longURL = "https://docs.spring.io/spring/docs/4.1.4.RELEASE/javadoc-api/org/springframework/jdbc/core/simple/SimpleJdbcTemplate.html";
        shortURL = "https://miniurl.com/Q5VHrix";
        bias = 951023546;
        Assert.assertEquals(shortURL, TinyURLArithmetic.getShortUrl(longURL,bias,length));
    }

    @Test
    public void testshorten() {
        longURL = "https://www.abcd.com/efgh"; bias = 0;
        Assert.assertEquals("kkjiqps", TinyURLArithmetic.shorten(longURL,bias,length));
    }

}
