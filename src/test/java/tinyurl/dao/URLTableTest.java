package tinyurl.dao;

import org.junit.*;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.SingleConnectionDataSource;

import java.util.Properties;

import static tinyurl.utilities.URLProperties.newProperties;

public class URLTableTest {

    URLTable testTable = new URLTable();
    public Properties props = newProperties();
    public String tableName = "url_test_table";
    public String attributesTypes = "(TimeStamp varchar, LongURL varchar, shortURL varchar primary key) ";
    public String attributes = "(TimeStamp, LongURL, shortURL) ";
    public JdbcTemplate jt;
    String pathLength = props.getProperty("jdbc.tinyURLPathLength");
    int length = Integer.parseInt(pathLength);

    @Before
    public void setUp() throws Exception {
        String driver = props.getProperty("jdbc.driver");
        String url = props.getProperty("jdbc.url");
        String username = props.getProperty("jdbc.username");
        String password = props.getProperty("jdbc.password");
        SingleConnectionDataSource ds = new SingleConnectionDataSource();
        ds.setDriverClassName(driver);
        ds.setUrl(url);
        ds.setUsername(username);
        ds.setPassword(password);
        jt = new JdbcTemplate(ds);
        String drop_command = "drop table if exists ";
        String exec = drop_command + tableName;
        jt.execute(drop_command+tableName);
        jt.execute("create table " + tableName + attributesTypes);
        jt.execute("insert into " + tableName + attributes + " values ('2020-07-25'," +
                " 'www.tabletest123.com', " + "'https://miniurl.com/EWYCltr') ");
        jt.execute("insert into " + tableName + attributes + " values ('2020-07-24'," +
                " 'www.tabletest456.com', " + "'https://miniurl.com/LFU2bwu') ");
        jt.execute("insert into " + tableName + attributes + " values ('2020-07-26'," +
                " 'www.tabletest789.com', " + "'https://miniurl.com/esFvCyx') ");
        testTable.setNewTable(jt, tableName);
    }

    @After
    public void shutDown() {
        jt.execute("DROP TABLE " + tableName);
    }

    @Test
    public void shortURLAccessTest() throws Exception {
        Assert.assertEquals("www.tabletest123.com",
                testTable.tryFetchURL("https://miniurl.com/EWYCltr"));
    }

    @Test
    public void longURLDuplicateTest() throws Exception {
        String longURL = "www.tabletest234.com";
        testTable.uniqueAddURL(longURL, "2020-07-23");
        testTable.uniqueAddURL(longURL, "2020-07-25");
        testTable.uniqueAddURL(longURL, "2020-07-23");
        String sql = "SELECT COUNT(longURL) FROM " + tableName +
                " WHERE longurl = '" + longURL + "'";

        int count = jt.queryForObject(sql, Integer.class);
        Assert.assertEquals(1,count);
    }

    @Test(expected = EmptyResultDataAccessException.class)
    public void testpurgeOld() throws Exception{
        String shortURL;
        String longURL = "www.abcde12345.com";
        shortURL = testTable.uniqueAddURL(longURL, "2020-05-23");
        try {
            testTable.purgeOld();
        } catch (Exception ex) {
            System.out.println(ex.toString());
        }
        testTable.tryFetchURL(shortURL);
    }

    @Test
    public void collisionTest() throws Exception {
        String shortURL1, shortURL2;
        String longURL1 = "www.abcde6789.com", longURL2 = "www.tabletest789.com";
        shortURL1 = testTable.uniqueAddURL(longURL1, "2020-08-23");
        shortURL2 = testTable.uniqueAddURL(longURL2, "2020-08-29");
        Assert.assertEquals(longURL1, testTable.tryFetchURL(shortURL1));
        Assert.assertEquals(longURL2, testTable.tryFetchURL(shortURL2));
    }
}
