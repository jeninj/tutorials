package tinyurl;

import com.google.gson.Gson;
import com.google.inject.Guice;
import com.google.inject.Injector;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.methods.RequestBuilder;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import tinyurl.models.URLGetResponse;
import tinyurl.models.URLPostResponse;
import tinyurl.modules.URLModule;

import java.io.ByteArrayOutputStream;

public class IntegrationTest {

    @BeforeClass
    public static void setup() throws Exception {
        Injector injector = Guice.createInjector(new URLModule());
        Server server = injector.getInstance(Server.class);
        ServletContextHandler handler = injector.getInstance(ServletContextHandler.class);
        server.start();
    }

    @Test
    public void testGet() throws Exception {
        HttpUriRequest request = RequestBuilder.create("GET")
                .setUri("http://localhost:7080/example/")
                .setEntity(new StringEntity("{\"URL\":\"https://miniurl.com/7YB6fys\"}", ContentType.APPLICATION_JSON))
                .build();
        Assert.assertEquals(1,1);
        HttpClient client = HttpClientBuilder.create().build();
        HttpResponse response = client.execute(request);
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        response.getEntity().writeTo(out);
        String jsonString = out.toString();
        Gson gson = new Gson();
        URLGetResponse urlResponse = gson.fromJson(jsonString, URLGetResponse.class);
        Assert.assertEquals(urlResponse.getURL(),"https://www.tryagain.com/remotehost");
    }

    @Test
    public void testPost() throws Exception {
        HttpUriRequest request = RequestBuilder.create("POST")
                .setUri("http://localhost:7080/example/")
                .setEntity(new StringEntity("{\"URL\":\"https://www.tryagain.com/remotehost\"}", ContentType.APPLICATION_JSON))
                .build();
        HttpClient client = HttpClientBuilder.create().build();
        HttpResponse response = client.execute(request);
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        response.getEntity().writeTo(out);
        String jsonString = out.toString();
        Gson gson = new Gson();
        URLPostResponse urlResponse = gson.fromJson(jsonString, URLPostResponse.class);
        Assert.assertEquals(urlResponse.getShortURL(),"https://miniurl.com/7YB6fys");
    }
}
