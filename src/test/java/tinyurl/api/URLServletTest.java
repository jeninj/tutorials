package tinyurl.api;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.SingleConnectionDataSource;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockHttpServletResponse;
import tinyurl.dao.URLTable;

import java.util.Properties;

import static tinyurl.utilities.URLProperties.newProperties;

public class URLServletTest {

    public URLTable testTable = new URLTable();
    public URLServlet testServlet = new URLServlet(testTable);
    public Properties props = newProperties();
    public String tableName = "url_test_table";
    public String attributesTypes = "(TimeStamp varchar, LongURL varchar, shortURL varchar primary key) ";
    public String attributes = "(TimeStamp, LongURL, shortURL) ";
    public JdbcTemplate jt;
    String pathLength = props.getProperty("jdbc.tinyURLPathLength");
    int length = Integer.parseInt(pathLength);
    private MockHttpServletRequest request;
    private MockHttpServletResponse response;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        String driver = props.getProperty("jdbc.driver");
        String url = props.getProperty("jdbc.url");
        String username = props.getProperty("jdbc.username");
        String password = props.getProperty("jdbc.password");
        SingleConnectionDataSource ds = new SingleConnectionDataSource();
        ds.setDriverClassName(driver);
        ds.setUrl(url);
        ds.setUsername(username);
        ds.setPassword(password);
        jt = new JdbcTemplate(ds);
        String drop_command = "drop table if exists ";
        String exec = drop_command + tableName;
        jt.execute(drop_command+tableName);
        jt.execute("create table " + tableName + attributesTypes);
        jt.execute("insert into " + tableName + attributes + " values ('2020-07-25'," +
                " 'www.tabletest123.com', " + "'https://miniurl.com/EWYCltr') ");
        jt.execute("insert into " + tableName + attributes + " values ('2020-07-24'," +
                " 'www.tabletest456.com', " + "'https://miniurl.com/LFU2bwu') ");
        jt.execute("insert into " + tableName + attributes + " values ('2020-07-26'," +
                " 'www.tabletest789.com', " + "'https://miniurl.com/esFvCyx') ");
        testTable.setNewTable(jt, tableName);
    }

    @Test
    public void doGetTest() {
        // Not Implemented
    }

    @After
    public void shutDown() {
        jt.execute("DROP TABLE " + tableName);
    }
}
