package tinyurl.api;

import com.google.gson.Gson;
import com.google.inject.Inject;
import org.apache.commons.io.IOUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.dao.EmptyResultDataAccessException;
import tinyurl.dao.URLTable;
import tinyurl.exceptions.OutofSpaceException;
import tinyurl.models.URLGetResponse;
import tinyurl.models.URLPostResponse;
import tinyurl.models.URLRequest;
import tinyurl.utilities.URLUtils;

import javax.servlet.AsyncContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Logger;

@WebServlet(urlPatterns = "/asyncServlet", asyncSupported = true)
public class URLAsyncExecutorServlet extends HttpServlet {

    public URLTable table;
    Gson gson;
    private static final Logger logger = Logger.getLogger(URLServlet.class.getName());;
    public ExecutorService executorService;

    @Inject
    public URLAsyncExecutorServlet(URLTable table) {
        this.table = table;
        gson = new Gson();
        executorService = Executors.newFixedThreadPool(10);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        AsyncContext asyncContext = request.startAsync(request, response);
        executorService.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    String CurrentTime = URLUtils.getTime();
                    String jsonString = IOUtils.toString(request.getInputStream());
                    URLRequest urlRequest = gson.fromJson(jsonString, URLRequest.class);
                    logger.info("Short URL Received: " + urlRequest.getURL());
                    String shortURL = urlRequest.getURL();
                    URLGetResponse urlResponse = new URLGetResponse();
                    String longURL = table.tryFetchAndUpdateURL(shortURL, CurrentTime);
                    urlResponse.setURL(longURL);
                    jsonString = gson.toJson(urlResponse);
                    JSONObject respJson = new JSONObject(jsonString);
                    response.setStatus(HttpServletResponse.SC_OK);
                    response.setContentType("application/json");
                    logger.info("Long URL Delivered: " + longURL);
                    response.getWriter().print(respJson);
                    response.getWriter().close();
                } catch (JSONException ex) {
                    logger.severe(ex.toString());
                    response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                } catch (EmptyResultDataAccessException ex) {
                    logger.severe(ex.toString());
                    response.setStatus(HttpServletResponse.SC_NOT_FOUND);
                } catch (Exception ex) {
                    logger.severe(ex.toString());
                    response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                }
                asyncContext.complete();
            }
        });
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        AsyncContext asyncContext = request.startAsync(request, response);
        executorService.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    String CurrentTime = URLUtils.getTime();
                    String jsonString = IOUtils.toString(request.getInputStream());
                    URLRequest urlRequest = gson.fromJson(jsonString, URLRequest.class);
                    logger.info("Long URL Received: " + urlRequest.getURL());
                    String URL = urlRequest.getURL();
                    String shortURL = table.uniqueAddURL(URL, CurrentTime);
                    URLPostResponse urlResponse = new URLPostResponse();
                    urlResponse.setURL(URL, shortURL);
                    jsonString = gson.toJson(urlResponse);
                    JSONObject respJson = new JSONObject(jsonString);
                    response.setStatus(HttpServletResponse.SC_OK);
                    response.setContentType("application/json");
                    logger.info("Short URL Delivered: " + shortURL);
                    response.getWriter().print(respJson);
                    response.getWriter().close();
                } catch (JSONException ex) {
                    logger.severe(ex.toString());
                    response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                } catch (OutofSpaceException ex) {
                    logger.severe(ex.toString());
                    response.setStatus(HttpServletResponse.SC_SERVICE_UNAVAILABLE);
                } catch (Exception ex) {
                    logger.severe(ex.toString());
                    response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                }
                asyncContext.complete();
            }
        });
    }

    public void customTableInitializer(URLTable table) {
        this.table = table;
    }
}
