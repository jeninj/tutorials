package tinyurl.url;

public class TinyURLArithmetic {
    public int numberOfCharacters = 62;

    public static String getShortUrl(String longURL, int bias, int length) {
        String value = shorten(longURL, bias, length);
        String output = "https://miniurl.com/" + value;
        return output;
    }

    public static String shorten(String longURL, int bias, int length) {
        int numberOfCharacters = 62; // UpperCase + LowerCase + Digits
        long b = shortenToLong(longURL, bias, length);
        char[] array = new char[length];
        for (int i = 0; i < length; i++) {
            array[i] = numberToString(b % numberOfCharacters);
            b = b / numberOfCharacters;
        }
        String output = new String(array);
        return output;
    }

    public static long shortenToLong(String longURL, int bias, int length) {
        char[] chars = longURL.toCharArray();
        long b = 0;
        int asciiStart = 32;
        int numberOfCharacters = 62; // UpperCase + LowerCase + Digits
        long div = Math.round(Math.pow(numberOfCharacters, length));
        for (char ch : chars) {
            int a = (byte) ch - asciiStart;
            b = (b * numberOfCharacters + a) % div;
        }
        b = (b + bias) % div;
        return b;
    }

    public static char numberToString(long input) {
        if ((input >= 0) && (input < 26)) {
            return (char) (input + 97);
        } else if ((input >= 26) && (input < 52)) {
            return (char) (input - 26 + 65);
        } else {
            return (char) (input - 52 + 48);
        }
    }
}