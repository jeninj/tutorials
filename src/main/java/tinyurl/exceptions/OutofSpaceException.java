package tinyurl.exceptions;

public class OutofSpaceException extends Exception {
    public OutofSpaceException(String s) {
        super(s);
    }
}
