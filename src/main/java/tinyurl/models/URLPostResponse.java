package tinyurl.models;

public class URLPostResponse {

    private String longURL;
    private String shortURL;

    public void setURL(String longURL, String shortURL) {
        this.longURL = longURL;
        this.shortURL = shortURL;
    }

    public String getShortURL() {
        return shortURL;
    }
}
