package tinyurl.models;

public class URLGetResponse {

    private String longURL;

    public String getURL() {
        return longURL;
    }

    public void setURL(String longURL) {
        this.longURL = longURL;
    }
}
