package tinyurl.dao;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.SingleConnectionDataSource;
import tinyurl.api.URLServlet;
import tinyurl.exceptions.OutofSpaceException;
import tinyurl.url.TinyURLArithmetic;
import tinyurl.utilities.URLUtils;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;
import java.util.Random;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

import static tinyurl.utilities.URLProperties.newProperties;

@Singleton
public class URLTable {

    public Properties props = newProperties();
    public String tableName = props.getProperty("jdbc.table");
    public String attributesTypes = "(TimeStamp varchar, LongURL varchar, shortURL varchar primary key) ";
    public String attributes = "(TimeStamp, LongURL, shortURL) ";
    public JdbcTemplate jt;
    String pathLength = props.getProperty("jdbc.tinyURLPathLength");
    int length = Integer.parseInt(pathLength);
    private static final Logger logger = Logger.getLogger(URLServlet.class.getName());
    private ScheduledExecutorService scheduledExecutorService;

    @Inject
    public URLTable() {
        try {
            String driver = props.getProperty("jdbc.driver");
            String url = props.getProperty("jdbc.url");
            String username = props.getProperty("jdbc.username");
            String password = props.getProperty("jdbc.password");

            SingleConnectionDataSource ds = new SingleConnectionDataSource();
            ds.setDriverClassName(driver);
            ds.setUrl(url);
            ds.setUsername(username);
            ds.setPassword(password);
            jt = new JdbcTemplate(ds);
            logger.info("Table Created");
            //scheduledPurge();
            this.scheduledExecutorService = Executors.newSingleThreadScheduledExecutor();
            this.scheduledExecutorService.schedule(this::purgeOld, 1, TimeUnit.MINUTES);
        } catch (Exception e) {
            e.printStackTrace();
            logger.severe(e.getClass().getName() + ": " + e.getMessage());
        }
    }

    public String uniqueAddURL(String longURL, String CurrentTime)
            throws OutofSpaceException {
        String shortURL;
        String newLongURL = null;
        Random rand = new Random(); //instance of random class
        long seed = TinyURLArithmetic.shortenToLong(longURL, 0, length) + 100;
        rand.setSeed(seed);
        for (int i = 0; i < 1000; i++) {
            int bias = rand.nextInt((int) Math.round(Math.pow(10, 9)));
            //int bias = 0; //Custom Bias
            shortURL = TinyURLArithmetic.getShortUrl(longURL, bias, length);
            try {
                TryAddURL(longURL, shortURL, CurrentTime);
            } catch (Exception ex) {
                newLongURL = tryFetchURL(shortURL);
                if (longURL.equals(newLongURL)) {
                    tryFetchAndUpdateURL(shortURL, CurrentTime);
                    return shortURL;
                }
                continue;
            }
            return shortURL;
        }
        throw new OutofSpaceException("Out of Space");
    }

    public String TryAddURL(String longURL, String shortURL, String CurrentTime) {
        String append = " values (?,?,?)";
        jt.update("insert into " + tableName + attributes + append, CurrentTime, longURL, shortURL);
        logger.info("Succesfully Added");
        return shortURL;
    }

    public String tryFetchURL(String shortURL) {
        String longURL = new String();
        String sql = "SELECT longURL FROM " + tableName + " WHERE shortURL = ?";
        longURL = (String) jt.queryForObject(sql, new Object[]{shortURL}, String.class);
        return longURL;
    }

    public String tryFetchAndUpdateURL(String shortURL, String CurrentTime) {
        String longURL = tryFetchURL(shortURL);
        String sql = "update " + tableName + " set timestamp = ? " +
                "where shorturl = ?";
        jt.update(sql, CurrentTime, shortURL);
        return longURL;
    }

    public void setNewTable(JdbcTemplate jt, String tableName) {
        this.jt = jt;
        this.tableName = tableName;
    }

    public void purgeOld() {
        try {
            logger.info("Scheduled Purge Underway");
            String myDate = URLUtils.getTime();
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
            sdf.setLenient(false);
            Date date = sdf.parse(myDate);
            long millisDate = date.getTime();
            int ttlDays = Integer.parseInt(props.getProperty("jdbc.ttlDays"));
            //long millisLast = 3*60*1000; //3 minutes to milliseconds
            long millisLast = URLUtils.daysToMilliSec(ttlDays);
            long millis = millisDate - millisLast;
            Date purgeDate = new Date(millis);
            Timestamp ts = new Timestamp(purgeDate.getTime());
            String purgeDeadline = ts.toString();
            String sql = "delete from " + tableName + " where timestamp<'"
                    + purgeDeadline + "'";
            jt.execute(sql);
            logger.info("Scheduled Purge Done");
        } catch (Exception ex) {
            logger.severe(ex.toString());
        }
    }
}
