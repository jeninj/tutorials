package tinyurl;

import com.google.inject.Guice;
import com.google.inject.Injector;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import tinyurl.modules.URLModule;

import java.util.logging.Logger;

public class Main {
    private static final Logger logger = Logger.getLogger(Main.class.getName());

    public static void main(String[] args) throws Exception {
        Injector injector = Guice.createInjector(new URLModule());
        Server server = injector.getInstance(Server.class);
        ServletContextHandler handler = injector.getInstance(ServletContextHandler.class);
        server.start();
        logger.info("Server started");
    }
}
