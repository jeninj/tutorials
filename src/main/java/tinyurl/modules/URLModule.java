package tinyurl.modules;

import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.google.inject.Singleton;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import tinyurl.api.URLAsyncExecutorServlet;
import tinyurl.dao.URLTable;

import java.util.Properties;

import static tinyurl.utilities.URLProperties.newProperties;

public class URLModule extends AbstractModule {

    @Provides
    @Singleton
    public Server provideServer() {
        Properties props = newProperties();
        int port = Integer.parseInt(props.getProperty("server.port"));
        return new Server(port);
    }

    @Provides
    @Singleton
    public ServletContextHandler provideServletContextHandler(Server server, URLTable table) {
        ServletContextHandler contextHandler = new ServletContextHandler(server, "/example");
        contextHandler.addServlet(new ServletHolder(new URLAsyncExecutorServlet(table)), "/");
        return contextHandler;
    }
}
