package tinyurl.utilities;

import java.sql.Timestamp;
import java.util.Date;

public class URLUtils {

    public static String getTime() {
        Date date = new Date();
        Timestamp ts = new Timestamp(date.getTime());
        String currentTime = ts.toString();
        return currentTime;
    }

    public static long daysToMilliSec(int days) {
        long millis;
        long longdays = days;
        millis = longdays*24*3600*1000;
        return millis;
    }

}
