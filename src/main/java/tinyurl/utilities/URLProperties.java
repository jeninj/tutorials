package tinyurl.utilities;

import com.google.inject.Singleton;
import tinyurl.Main;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.logging.Logger;

@Singleton
public class URLProperties {
    private static final Logger logger = Logger.getLogger(URLProperties.class.getName());

    public static Properties newProperties() {
        String environment;
        String env = System.getenv("env");
        String local = "local";
        logger.info("Environment variable is: " + env);
        if (local.equals(env)) {
            environment = "local";
        } else {
            environment = "staging";
        }
        Properties props = new Properties();
        String FilePath = environment + "Settings.properties";
        ClassLoader classLoader = Main.class.getClassLoader();
        try {
            InputStream in = classLoader.getResourceAsStream(FilePath);
            props.load(in);
        } catch (IOException ex) {
            logger.severe(ex.toString());
        }
        return props;
    }
}
