## URL Shortener

Converts a large URL into a tiny URL (27 characters). Uses API to convert large URLs and retrieve previously converted tiny URL stored in a database. 
Utilizes the following API Operations -

- CREATE : Creates a 7 character code (UpperCase + LowerCase + Digits) & returns a corresponding URL. 
            Stores the large URL and the tiny URL in a db.
- GET : Given the tiny URL, returns the previously stored large URL.
- PURGE : The db is purged of entries last accessed before 30 days. The purge runs in the background & is repeated every 10 minutes. 

### Steps
1. Alter the src/main/resources/db.properties according to local database server. The Server API currently POSTs & GETs from "http://localhost:8080/example/"
2. Start your PostgresSQL databse server. Run src/main/resources/start.sql  
3. Build and Run with Maven
4. Use Postman to POST & GET API Requests from the server
